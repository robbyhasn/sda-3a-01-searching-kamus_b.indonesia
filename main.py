import json, os
from typing import Text
from googletrans import Translator	
from termcolor import colored
try: # Windows
    from msvcrt import getch, getche
except ImportError: # Linux
    def getch():
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch
    def getche():
        ch = getch()
        print(ch, end='')
        return ch

if __name__ == "__main__":
	# Opening JSON file
	f = open('data.json')

	# returns JSON object as
	# a dictionary
	data = json.load(f)

	# Logica searching algorithm
	while True:
		os.system('cls')
		print(colored("\n================================", "green"))
		print(colored("PROGRAM KAMUS BAHASA INDONESIA", "yellow"))
		print(colored("================================", "green"))
		print(colored("Menu: ", "yellow"))
		print(colored("1. Search berdasarkan kata dasar ", "yellow"))
		print(colored("2. Search berdasarkan kode", "yellow"))
		print(colored("3. Translate", "yellow"))
		print(colored("4. About", "yellow"))
		print(colored("5. Exit", "yellow"))
		opsi = input(colored(">> ", "red"))
		
		if opsi == "1":
			search = input("Search : ")
			for item in data:
				if item["kata_dasar"].lower() == search.lower():
					print(item['kode'])	
					print(item['deskripsi'])
					break
			else:
				print("Tidak ditemukan")

			getch()
			continue

		elif opsi == "2":
			search = input("Search : ")
			for item in data:
				if item["kode"] == search:
					print(item['kata_dasar'])
					print(item['deskripsi'])
					break
			else:
				print("Tidak ditemukan")

			getch()
			continue

		elif opsi == "3":
			translator = Translator()
			print("Pilih bahasa:")
			print("(ar) Arab")
			print("(af) Afrikans")
			print("(it) Italian")
			print("(ja) Japanese")
			print("(kn) Kannada")
			print("(ko) Korean")
			print("(bn) Benggali")
			print("(la) Latin")
			print("(bg) Bulgarian")
			print("(zh-CN) Chiinese Simplified")
			print("(zh-TW) Chinese Traditional")
			print("(pt) Portuguese")
			print("(en) English")
			print("(ro) Romanian")
			print("(ru) Russian")
			print("(sr) Serbian")
			print("(tl) Filipino")
			print("(fr) French")
			print("(es) Spanish")
			print("(de) German")
			bahasa = str(input("Masukan kode negara :  "),)
			text = input("Masukan text : ")
			hasil = translator.translate(text, dest=bahasa)

			if bahasa == "ar":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "af":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "it":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "ja":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "kn":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "ko":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "bn":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "la":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "bg":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "zh-CN":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "zh-TW":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "pt":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "en":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "ro":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "ru":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "sr":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "tl":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "fr":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "es":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			elif bahasa == "de":
				print(">> ", hasil.src, text)
				print(">> ", hasil.dest, hasil.text)
				print("Talk : ", hasil.pronunciation)

				getch()
				continue

			else:
				print("Bahasa tidak tersedia !")
				break
			

				


		elif opsi == "4":
			print('Program ini dibuat untuk menyelesaikan tugas akhir mata kuliah STRUKTUR DATA')
			print()
			print("                              Dibuat Oleh:                                  ")
			print("                     Robby Hasan N (11200910000019)                         ")
			print("                     Nurrani Afifah (11200910000012)                        ")


			getch()
			continue


		elif opsi == "5":
			break

	# Closing file
	f.close()